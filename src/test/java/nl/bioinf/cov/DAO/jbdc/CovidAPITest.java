package nl.bioinf.cov.DAO.jbdc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nl.bioinf.cov.CovApplication;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;

/**
 * @author Hans Zijlstra
 */
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CovApplication.class)
@AutoConfigureMockMvc
class CovidAPITest {


    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("build/generatedsnippets");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    /**
     Mocks an object starting the webAppcontext
     */

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .apply(documentationConfiguration(restDocumentation)).build();
    }

    /**
     Test method testing the API, for the url: /covid19/{country}?page=1&size=5
     documents link automatically, setting parameters and responsefields
     */
    @Test
    void getDataResultsByCountry() throws Exception {
        String jsonResponse = "{\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/covid19/china?page=1&size=5\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19/china?page=2&size=5\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19/china?page=0&size=5\"}],\"content\":[{\"province\":\"Anhui\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":990,\"deaths\":6,\"recovered\":984},{\"province\":\"Jiangxi\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":935,\"deaths\":1,\"recovered\":932},{\"province\":\"Shandong\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":760,\"deaths\":6,\"recovered\":726},{\"province\":\"Jiangsu\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":631,\"deaths\":0,\"recovered\":627},{\"province\":\"Chongqing\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":576,\"deaths\":6,\"recovered\":554}]}//localhost:8080/covid19/china?page=1&size=5\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19/china?page=2&size=5\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19/china?page=0&size=5\"}]},{\"province\":\"Jiangxi\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":935,\"deaths\":1,\"recovered\":932,\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/covid19/china?page=1&size=5\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19/china?page=2&size=5\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19/china?page=0&size=5\"}]},{\"province\":\"Shandong\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":760,\"deaths\":6,\"recovered\":726,\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/covid19/china?page=1&size=5\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19/china?page=2&size=5\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19/china?page=0&size=5\"}]},{\"province\":\"Jiangsu\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":631,\"deaths\":0,\"recovered\":627,\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/covid19/china?page=1&size=5\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19/china?page=2&size=5\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19/china?page=0&size=5\"}]},{\"province\":\"Chongqing\",\"country\":\"China\",\"lastUpdate\":\"2020-03-11\",\"confirmedInfected\":576,\"deaths\":6,\"recovered\":554,\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/covid19/china?page=1&size=5\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19/china?page=2&size=5\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19/china?page=0&size=5\"}]}]";
        ResultMatcher ok = MockMvcResultMatchers.status().isOk();
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/covid19/{country}?page=1&size=5", "china")
        .accept(MediaType.APPLICATION_JSON)).andExpect(ok).andExpect(MockMvcResultMatchers.content().json(jsonResponse))

                .andDo(document("covid19",
                        links(
                            linkWithRel("self").description("Link to self"),
                            linkWithRel("next").description("Link to next"),
                            linkWithRel("previous").description("Link to next")),
                        pathParameters(
                                parameterWithName("country").description("The country to retrieve")
                        ),
                        responseFields(
                                fieldWithPath("content[].deaths").description("Amount of total deaths"),
                                fieldWithPath("content[].province").description("Province of submitted country"),
                                fieldWithPath("content[].country").description("The submitted country"),
                                fieldWithPath("content[].lastUpdate").description("Date, data submitions"),
                                fieldWithPath("content[].confirmedInfected").description("People infected with Covid19"),
                                fieldWithPath("content[].recovered").description("People recovered from covid 19"),
                                subsectionWithPath("links").description("links"))));

    }

    /**
     Test method testing the API, for the url: /covid19/deaths/{country}
     documents link automatically, setting parameters and responsefields
     */
    @Test
    void getConfirmedDeathsCountry() throws Exception {
        String jsonResponse = "[{\"country\":\"China\",\"deaths\":4512}]";
        ResultMatcher ok = MockMvcResultMatchers.status().isOk();
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/covid19/deaths/{country}", "china"))
                .andExpect(ok).andExpect(MockMvcResultMatchers.content().json(jsonResponse))
                .andDo(document("ConfirmedDeathsCountry",
                        pathParameters(
                                parameterWithName("country").description("submitted country")
                        ),
                        responseFields(
                                fieldWithPath("[].deaths").description("Amount of total deaths"),
                                fieldWithPath("[].country").description("The submitted country"))));
    }

    /**
     Test method testing the API, for the url: /covid19/deaths/{country}
     documents link automatically, setting parameters and responsefields
     */
    @Test
    void getConfirmedDeathsCountryException() throws Exception {
        ResultMatcher badRequest = MockMvcResultMatchers.status().isBadRequest();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/covid19/deaths/{country}", "sadasdasd")
                .accept(MediaType.APPLICATION_JSON)).andExpect(badRequest).andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    /**
     Test method testing the API, for a bad request
     */
    @Test
    void getconfirmedCasesByDate() throws Exception {
        String jsonResponse = "{\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/covid19?fromDate=2020-01-25&endDate=2020-01-28&country=US&page=0&size=10\"},{\"rel\":\"next\",\"href\":\"http://localhost:8080/covid19?fromDate=2020-01-25&endDate=2020-01-28&country=US&page=1&size=10\"},{\"rel\":\"previous\",\"href\":\"http://localhost:8080/covid19?fromDate=2020-01-25&endDate=2020-01-28&country=US&page=-1&size=10\"}],\"content\":[{\"province\":\"Washington\",\"country\":\"US\",\"lastUpdate\":\"2020-01-25\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"Illinois\",\"country\":\"US\",\"lastUpdate\":\"2020-01-25\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"Washington\",\"country\":\"US\",\"lastUpdate\":\"2020-01-26\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"Illinois\",\"country\":\"US\",\"lastUpdate\":\"2020-01-26\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"California\",\"country\":\"US\",\"lastUpdate\":\"2020-01-26\",\"confirmedInfected\":2,\"deaths\":0,\"recovered\":0},{\"province\":\"Arizona\",\"country\":\"US\",\"lastUpdate\":\"2020-01-26\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"Washington\",\"country\":\"US\",\"lastUpdate\":\"2020-01-27\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"Illinois\",\"country\":\"US\",\"lastUpdate\":\"2020-01-27\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0},{\"province\":\"California\",\"country\":\"US\",\"lastUpdate\":\"2020-01-27\",\"confirmedInfected\":2,\"deaths\":0,\"recovered\":0},{\"province\":\"Arizona\",\"country\":\"US\",\"lastUpdate\":\"2020-01-27\",\"confirmedInfected\":1,\"deaths\":0,\"recovered\":0}]}";

        ResultMatcher ok = MockMvcResultMatchers.status().isOk();

        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/covid19?fromDate=2020-01-25&endDate=2020-01-28&country=US")
                .accept(MediaType.APPLICATION_JSON)).andExpect(ok).andExpect(MockMvcResultMatchers.content().json(jsonResponse))
                .andDo(document("ConfirmedCasesBydate",
                        links(
                                linkWithRel("self").description("Link to self"),
                                linkWithRel("next").description("Link to next"),
                                linkWithRel("previous").description("Link to next")),
                        requestParameters(
                                parameterWithName("fromDate").description("start date range"),
                                parameterWithName("endDate").description("end date range"),
                                parameterWithName("country").description("submitted country")
                        ),
                        responseFields(
                                fieldWithPath("content[].deaths").description("Amount of total deaths"),
                                fieldWithPath("content[].province").description("Province of submitted country"),
                                fieldWithPath("content[].country").description("The submitted country"),
                                fieldWithPath("content[].lastUpdate").description("Date, data submitions"),
                                fieldWithPath("content[].confirmedInfected").description("People infected with Covid19"),
                                fieldWithPath("content[].deaths").description("Deaths due to covid 19"),
                                fieldWithPath("content[].recovered").description("People recovered from covid 19"),
                                subsectionWithPath("links").description("links"))));
    }
}