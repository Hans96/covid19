package nl.bioinf.cov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovApplication {

    public static void main(String[] args) {
        SpringApplication.run(CovApplication.class, args);
    }

}
