package nl.bioinf.cov.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.ResourceSupport;

/**
 * @author Hans Zijlstra
 * Class representing Covid 19 statistics including, deaths due to covid, number of people recovered, number of people
 * infected. Other statistics include start date and end date.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CovidResults {
    private String province;
    private String country;
    private String lastUpdate;
    private int confirmedInfected;
    private int deaths;
    private int recovered;

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public int getConfirmedInfected() {
        return confirmedInfected;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getRecovered() {
        return recovered;
    }

    private CovidResults(Builder builder) {
        this.province = builder.province;
        this.country = builder.country;
        this.lastUpdate = builder.lastUpdate;
        this.confirmedInfected = builder.confirmedInfected;
        this.deaths = builder.deaths;
        this.recovered = builder.recovered;
    }
    /**
     * Static method that serves an instance of the inner class Builder
     * @return ProductDescriptionBuilder object
     */
    public static class Builder {
        private String province;
        private String country;
        private String lastUpdate;
        private int confirmedInfected;
        private int deaths;
        private int recovered;

        public Builder province(String province) {
            this.province = province;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder lastUpdate(String lastUpdate) {
            this.lastUpdate = lastUpdate;
            return this;
        }

        public Builder confirmedInfected(int confirmedInfected) {
            this.confirmedInfected = confirmedInfected;
            return this;
        }

        public Builder deaths(int deaths) {
            this.deaths = deaths;
            return this;
        }

        public Builder recovered(int recovered) {
            this.recovered = recovered;
            return this;
        }

        public CovidResults build() {
            return new CovidResults(this);

        }


    }

    public static Builder builder() {
        return new Builder();
    }
}
