package nl.bioinf.cov.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

/**
 * @author Hans Zijlstra
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CovidCountries{
    private String province;
    private String country;
    private String lastUpdate;
    private int confirmedInfected;
    private int deaths;
    private int recovered;

    public CovidCountries(String province, String country, String lastUpdate, int confirmedInfected, int deaths, int recovered) {
        this.province = province;
        this.country = country;
        this.lastUpdate = lastUpdate;
        this.confirmedInfected = confirmedInfected;
        this.deaths = deaths;
        this.recovered = recovered;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public int getConfirmedInfected() {
        return confirmedInfected;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getRecovered() {
        return recovered;
    }

}
