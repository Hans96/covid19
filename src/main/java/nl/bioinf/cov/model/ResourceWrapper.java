package nl.bioinf.cov.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hans Zijlstra
 * Resource wraps object to serve as
 */
public class ResourceWrapper {
    List<Object> wrappedObject = new ArrayList<>();

    public List<Object> getCovidWrapper() {
        return wrappedObject;
    }

}
