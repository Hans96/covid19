package nl.bioinf.cov.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

/**
 * @author Hans Zijlstra
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmedDeaths{
    private String country;
    private int deaths;

    public ConfirmedDeaths(String country, int deaths) {
        this.country = country;
        this.deaths = deaths;
    }

    public String getCountry() {
        return country;
    }

    public int getDeaths() {
        return deaths;
    }

    public void add(Link selfRelLink) {
    }
}
