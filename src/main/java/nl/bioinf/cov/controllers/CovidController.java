package nl.bioinf.cov.controllers;

import nl.bioinf.cov.DAO.CovidRepository;
import nl.bioinf.cov.model.ConfirmedDeaths;
import nl.bioinf.cov.model.CovidCountries;
import nl.bioinf.cov.model.CovidResults;
import nl.bioinf.cov.model.ResourceWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Hans Zijlstra
 * Controller class that requests data from the covidRepositoy database, this data is mapped to a
 * response entity object. When the data is valid this object is passed to the prefered model
 * This controller classlistens to the /covid19/{country} request, when called the statistics for the
 * requested country are returned
 */


@RestController
public class CovidController {

    @Autowired
    private CovidRepository covidRepository;

    /**
     * Creates a Resources object containing storing An object of CovidCountries containing covid statistics
     * for the requested country when /covid/{country} is requested. Adds pointer links to the resulset.
     * @param country country to fetch covid statistics by
     * @param page returns the number of pages.
     * @param size sets the number of results for the pages.
     * @return Responsee ntity storing Resources Object.
     */
    @GetMapping(value = "/covid19/{country}")
    public ResponseEntity<Object> covidCases(@PathVariable String country, @RequestParam(defaultValue = "0") Integer page,
                                             @RequestParam(defaultValue = "10") Integer size) {

        try {
            Resources wrapper;
            Link selfRelLink = ControllerLinkBuilder.linkTo(
                    ControllerLinkBuilder
                            .methodOn(CovidController.class)
                            .covidCases(country, page, size)).withSelfRel();
            Link nextRelLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CovidController.class)
                    .covidCases(country, page + 1, size))
                    .withRel("next");
            Link previousRelLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CovidController.class)
                    .covidCases(country, page - 1, size))
                    .withRel("previous");


            List<CovidCountries> covidCountries = covidRepository.getDataResultsByCountry(country, page, size);

            if (covidCountries.size() == 0) {
                throw new IllegalArgumentException();
            } else {
                wrapper = new Resources(covidCountries);
                wrapper.add(selfRelLink);
                wrapper.add(nextRelLink);
                wrapper.add(previousRelLink);

                }

            return new ResponseEntity<>(wrapper, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Data could not be found");
        }


    }

    /**
     * Creates a Resources object containing storing An object of confirmedDeaths containing the number of fatalities
     * for the requested country when /covid19/deaths/{country} is requested. Adds pointer links to the resultset.
     * @param country country to fetch covid statistics by
     * @param page returns the number of pages.
     * @param size sets the number of results for the pages.
     * @return Response entity storing Resources Object.
     */
    @GetMapping("/covid19/deaths/{country}")
    public ResponseEntity<Object> deathsPerCountry(@PathVariable String country, @RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "10") Integer size) {
        try {
            ResourceWrapper wrapper = new ResourceWrapper();
            Link selfRelLink = ControllerLinkBuilder.linkTo(
                    ControllerLinkBuilder
                            .methodOn(CovidController.class)
                            .deathsPerCountry(country, page, size)).withSelfRel();
            Link nextRelLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CovidController.class)
                    .deathsPerCountry(country, page + 1, size))
                    .withRel("next");
            Link previousRelLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CovidController.class)
                    .deathsPerCountry(country, page - 1, size))
                    .withRel("previous");


            List<ConfirmedDeaths> confirmedDeaths = covidRepository.getConfirmedDeathsCountry(country, page, size);

            if (confirmedDeaths.get(0).getCountry() == null && confirmedDeaths.get(0).getDeaths() == 0) {
                throw new IllegalArgumentException();
            } else {
                for (ConfirmedDeaths deaths : confirmedDeaths) {
                    deaths.add(selfRelLink);
                    deaths.add(nextRelLink);
                    deaths.add(previousRelLink);
                    wrapper.getCovidWrapper().add(deaths);

                }

            }
            return new ResponseEntity<>(confirmedDeaths, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Country has not been found");
        }
    }

    /**
     * Creates a Resources object containing storing An object of CovidResults containing the number of fatalities
     * for the requested country when /covid19/is requested. Adds pointer links to the resultset.
     * @param country country to fetch covid statistics by
     * @param fromDate begin date to fetch statistics from
     * @param endDate end date to fetch statistics from
     * @param page returns the number of pages.
     * @param size sets the number of results for the pages.
     * @return Response entity storing Resources Object.
     */
    @GetMapping("/covid19")
    public ResponseEntity<Object> getDeathsBydate(
            @RequestParam(value = "fromDate") String fromDate,
            @RequestParam(value = "endDate") String endDate,
            @RequestParam(value = "country") String country,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {


        try {
            Resources wrapper;
            Link selfRelLink = ControllerLinkBuilder.linkTo(
                    ControllerLinkBuilder
                            .methodOn(CovidController.class)
                            .getDeathsBydate(fromDate, endDate,country, page, size)).withSelfRel();
            Link nextRelLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CovidController.class)
                    .getDeathsBydate(fromDate, endDate,country, page + 1, size))
                    .withRel("next");
            Link previousRelLink = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CovidController.class)
                    .getDeathsBydate(fromDate, endDate,country, page -1, size))
                    .withRel("previous");

            List<CovidResults> deathsByDate = covidRepository.getconfirmedCasesByDate(fromDate, endDate, country,  page, size);

            if (deathsByDate.size() == 0) {
                throw new IllegalArgumentException();
            } else {
                wrapper = new Resources(deathsByDate);
                wrapper.add(selfRelLink);
                wrapper.add(nextRelLink);
                wrapper.add(previousRelLink);
            }
                return new ResponseEntity<>(wrapper, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No data found for your query");
        }
    }



}
