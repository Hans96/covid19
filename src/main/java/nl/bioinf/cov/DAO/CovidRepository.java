package nl.bioinf.cov.DAO;

import nl.bioinf.cov.model.ConfirmedDeaths;
import nl.bioinf.cov.model.CovidCountries;
import nl.bioinf.cov.model.CovidResults;


import java.util.List;

/**
 * @author Hans Zijlstra
 */

public interface CovidRepository {

   List<CovidCountries> getDataResultsByCountry(String country, int pageNo, int pageSize);

   List<ConfirmedDeaths> getConfirmedDeathsCountry(String Country, int pageNo, int pageSize);

   List<CovidResults> getconfirmedCasesByDate(String fromDate, String endDate,  String country, int pageNo, int pageSize);


}
