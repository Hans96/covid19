package nl.bioinf.cov.DAO.jbdc;

import nl.bioinf.cov.DAO.CovidRepository;
import nl.bioinf.cov.model.ConfirmedDeaths;
import nl.bioinf.cov.model.CovidCountries;
import nl.bioinf.cov.model.CovidResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author Hans Zijlstra
 *  * Class which funtions as a Data Accesss Object to retrieve data from the database.
 */

@Repository
public class CovidDAO implements CovidRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CovidDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Method that retrieves a newProduct object from the database
     * @param country Country to fetch from database
     * @param pageNo set number of pages
     * @param pageSize sets the size of the returned pages
     * @return List of CovidCountries object
     */

    @Override
    public List<CovidCountries> getDataResultsByCountry(String country, int pageNo, int pageSize) {
        String sqlQuery = "select Province_State, Country_Region, Last_Update_Date, Confirmed, Deaths, Recovered from covid_reports where Country_Region = ? order by Last_Update_Date limit ?, ?";
        return jdbcTemplate.query(sqlQuery, new Object[] { country, pageNo*pageSize, pageSize }, new RowMapper<CovidCountries>() {
            @Override
            public CovidCountries mapRow(ResultSet resultSet, int i) throws SQLException {
                return new CovidCountries(resultSet.getString("Province_State"), resultSet.getString("Country_Region"), resultSet.getString("Last_Update_Date"),
                        resultSet.getInt("Confirmed"), resultSet.getInt("Deaths"), resultSet.getInt("Recovered"));
            }
        });
    }

    /**
     * Method that retrieves  data from databasee and maps this to the confirmedDeaths object
     * @param country Country to fetch from database
     * @param pageNo set number of pages
     * @param pageSize sets the size of the returned pages
     * @return List of ConfirmedDeaths object
     */
    @Override
    public List<ConfirmedDeaths> getConfirmedDeathsCountry(String country, int pageNo, int pageSize) {
        String sqlQuery = "select Country_Region, Max(Deaths) as result from covid_reports where Country_Region = ? and Last_Update_Date = (select MAX(Last_Update_Date) from covid_reports) limit ?, ?;";
        return jdbcTemplate.query(sqlQuery, new Object[]{country, pageNo, pageSize}, new RowMapper<ConfirmedDeaths>() {
            @Override
            public ConfirmedDeaths mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new ConfirmedDeaths(rs.getString("Country_Region"), rs.getInt("result"));
            }
        });

    }

    /**
     * Method that retrieves  data from databasee and maps this to the confirmedDeaths object
     * @param country Country to fetch from database
     * @param fromDate Start date to fetch statistics from
     * @param endDate End date to fetch statistics from
     * @param pageNo set number of pages
     * @param pageSize sets the size of the returned pages
     * @return List of CovidResults object
     */
    @Override
    public List<CovidResults> getconfirmedCasesByDate(String fromDate, String endDate,  String country, int pageNo, int pageSize) {
        String sqlQuery = "SELECT Province_State, Country_Region, Last_Update_Date, Confirmed, Deaths, Recovered from covid_reports where Last_Update_Date  between ? and ? and Country_Region = ? limit ?, ?";
        return jdbcTemplate.query(sqlQuery, new Object[]{fromDate, endDate, country, pageNo, pageSize}, new RowMapper<CovidResults>() {
            @Override
            public CovidResults mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new CovidResults.Builder().country(rs.getString("Country_Region")).deaths(rs.getInt("Deaths")).confirmedInfected(rs.getInt("Confirmed")).province(rs.getString("Province_State")).lastUpdate(rs.getString("Last_Update_Date")).recovered(rs.getInt("Recovered")).build();

            }
        });
    }
}
